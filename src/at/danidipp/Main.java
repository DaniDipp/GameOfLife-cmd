package at.danidipp;

public class Main {

    private static boolean[][] world;
    private static final String[] START = {
            ". . . . . . . . . . ",
            ". . . . . . . . . . ",
            ". . . . . . . . . . ",
            ". . . . * . . . . . ",
            ". . . . . * . . . . ",
            ". . . * * * . . . . ",
            ". . . . . . . . . . ",
            ". . . . . . . . . . ",
            ". . . . . . . . . . ",
            ". . . . . . . . . . "
    };
    private static final char DEAD_CELL = '.';
    private static final char ALIVE_CELL = '*';
    private static final int STEPS = 5;

    public static void main(String[] args) {
        init();
        printWorld();
        for(int i=0; i<STEPS; i++){
            step();
            printWorld();
        }
    }

    private static void init(){
        for(int i = 0; i< START.length; i++){
            START[i] = START[i].replaceAll("\\s+",""); //remove all spaces
        }

        boolean[][] newWorld = new boolean[START.length][START[0].length()];
        for(int i = 0; i< START.length; i++){
            for(int j = 0; j< START[i].length(); j++){
                if(START[i].charAt(j) == '.')   newWorld[i][j] = false;
                else                            newWorld[i][j] = true;
            }
        }

        world = newWorld;
    }

    private static void step(){
        boolean[][] newWorld = new boolean[world[0].length][world.length];
        for(int i=0; i<world.length; i++){
            for(int j=0; j<world[i].length; j++){
                int neighbors = countAliveNeighbors(i, j);

                if(world[i][j]){
                    if(neighbors<2) newWorld[i][j] = false;                     //Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
                    if(neighbors == 2 || neighbors ==3) newWorld[i][j] = true;  //Any live cell with two or three live neighbours lives on to the next generation.
                    if(neighbors>3) newWorld[i][j] = false;                     //Any live cell with more than three live neighbours dies, as if by overpopulation.
                }
                else if(neighbors == 3) newWorld[i][j] = true;                  //Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
                else{
                    newWorld[i][j] = world[i][j]; // Do nothing
                }
            }
        }
        world = newWorld;
    }

    private static int countAliveNeighbors(int x, int y){
        int aliveNeighbors = 0;
        for(int i=x-1; i<=x+1; i++){
            for(int j=y-1; j<=y+1; j++){
                if(i<0 || i>world.length-1 || j<0 || j>world.length-1 || (i==x && j==y)) continue;
                if(world[i][j] == true) aliveNeighbors++;
            }
        }
        return aliveNeighbors;
    }

    private static void printWorld(){
        for(int i=0; i<world.length; i++){
            for(int j=0; j<world[i].length; j++){
                System.out.print((world[i][j] ? ALIVE_CELL : DEAD_CELL)+" ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
